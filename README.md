# Scio

The word _scio_ in Esperanto - a constructed, international language to foster international understanding - means "knowledge".

![Scio Splash](Images/Scio_Image.png)

## Purpose

The purpose of this project - and, of all the projects in the [GiLab Scio](https://gitlab.com/gitlab-scio) Group - is to provide easier access to knowledge - knowledge pertaining to GitLab, its capabilities, how it enables DevOps in general, and how it integrates to other solutions.

The "easier access to knowledge" is provided primarily in the form of an [wiki](https://gitlab.com/gitlab-scio/gitlab-resources/-/wikis/A:-Table-of-Contents) - an index aggregating links to resources, documentation, blog posts, videos, and more - grouped logically, making it easier to find information.  Think of this wiki as a _Google Assistant_ - with a large amount of information available, we have gathered relevant resources to help you find what you need quickly.

## Contact

For questions, suggestions, updates, or corrections, please create an issue and mention:

- [Pete DiStefano](https://gitlab.com/pfdistef85_git) - Solutions Architect
- [Victor Hernandez](https://gitlab.com/vehernandez) - Solutions Architect

### Attributions

[Scio Group](https://gitlab.com/gitlab-scio) and [GitLab Resources](https://gitlab.com/gitlab-scio/gitlab-resources) Logo and Image <a href="http://www.freepik.com">designed by rawpixel.com / Freepik</a>
